/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tn_hash_tags', {
    hash_tag_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    hash_tag_name: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    timestamps : false,
    schema: 'discovery_tmp',
    tableName: 'tn_hash_tags'
  });
};
