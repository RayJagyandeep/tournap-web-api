/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tn_users', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_name: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    user_email: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    user_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_blog_url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    user_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    user_email_verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    user_tn_token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_fb_token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_g_token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_tw_token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    account_status: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    timestamps: false,
    schema : 'discovery_tmp',
    tableName: 'tn_users'
  });
};
