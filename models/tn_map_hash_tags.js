/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tn_map_hash_tags', {
    hash_tag_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(discovery_tmp.tn_map_hash_tags_hash_tag_id_seq::regclass)'
    },
    place_tag_ids: {
      type: DataTypes.JSON,
      allowNull: false
    },
    link_ids: {
      type: DataTypes.JSON,
      allowNull: false
    }
  }, {
    timestamps: false,
    schema: 'discovery_tmp',
    tableName: 'tn_map_hash_tags'
  });
};
