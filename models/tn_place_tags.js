/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tn_place_tags', {
    place_tag_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    place_tag_name: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    timestamps : false,
    schema : 'discovery_tmp',
    tableName: 'tn_place_tags'
  });
};
