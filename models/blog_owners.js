/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('blog_owners', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    blog_url: {
      type: DataTypes.STRING,
      allowNull: true
    },
    blog_owner: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: false,
    schema : 'discovery_tmp',
    tableName: 'blog_owners'
  });
};
