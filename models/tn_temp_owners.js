/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tn_temp_owners', {
    owner_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    owner_name: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    owner_email: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    timestamps: false,
    schema: 'discovery_tmp',
    tableName: 'tn_temp_owners'
  });
};
