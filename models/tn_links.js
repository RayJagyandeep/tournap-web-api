/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tn_links', {
    link_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    link_title: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    link_url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    link_image_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    link_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    link_is_video: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    link_is_place_specific: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    link_is_business: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    link_owner: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    link_is_claimed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    link_tags: {
      type: DataTypes.JSON,
      allowNull: false
    },
    link_places: {
      type: DataTypes.JSON,
      allowNull: false
    },
    link_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    timestamps: false,
    schema : 'discovery_tmp',
    tableName: 'tn_links'
  });
};
