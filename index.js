var restify = require('restify');
var morgan = require('morgan');
var db = require('./models/index');
var routes = require('./src/routes/index');
var bodyParser = require('body-parser');

var server = restify.createServer();
server.use(morgan('combined'));
server.use(bodyParser.urlencoded({extended:false}));
server.use(bodyParser.json());

routes.applyRoutes(server);

server.listen(8080,function(){
    db.sequelize.authenticate().then(() => {
        console.log("Database Connected successfully");
    }).catch(err => {
        console.err("Unable to connect to database");
    });
    console.log("Server is up and running on port 8080");
});