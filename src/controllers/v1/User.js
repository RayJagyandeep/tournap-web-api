var { User, UserProfile } = require('../../../models/index');
var bcrypt = require('bcrypt');
const { getValues } = require('sequelize-values')();
var cnf = require('../../../config/appConfig');
const { setReturnObject, generateToken, getError, parseQuery, sendEmail } = require('../../helper/utils');
var jwt = require('jsonwebtoken');
var err = require('restify-errors');
const _ = require('lodash');

async function signUp(req,res, next) {
   const{
       user_name,
       email,
       password
   } = req.body;
   
   try{
       
    var uData = getValues(await User.create({
        user_name            : user_name,
        user_email           : email,
        user_password        : bcrypt.hashSync(password,cnf.SALT_ROUND),
        user_created_at      : new Date(),
        user_blog_url        : "",
        user_email_verified  : false,
        user_tn_token        : "",
        user_fb_token        : "",
        user_g_token         : "",
        user_tw_token        : "",
        account_status       : true,
        user_updated_at      : new Date()
    }));

    UserProfile.create({
        user_id:uData.id
    });
    var confirmKey = generateToken(req.body, 'confirmEmail');
    var parts = req.url.split('/');
    var confirmLink = "http://"+req.headers.host+'/'+parts[1]+'/'+parts[2]+'/confirm-email?key='+confirmKey;
    console.log(confirmLink);
    res.send(200, setReturnObject(200,cnf.SUCCESS.SUCCESS_CODE,cnf.SUCCESS.SUCCESS_MESSAGE));
    return next();
   } catch(err){
       console.log(err);
       return next(getError(err));
   }
}
async function signIn(req,res,next){
    const {
        id
    } = req.User;

    const returnData = {};

    returnData.access_token = await generateToken(req.User,'access');
    returnData.refresh_token = await generateToken(req.User,'refresh');
    
    try{
        User.update({
            user_tn_token : returnData.refresh_token
        },{
            where: {id : id}
        });
        
        res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE, returnData));
        return next();
    } catch(err){
        return next(getError(err));
    }
}

async function getUsers(req,res,next){
    var userData = await User.findAll();
    var userValues = getValues(userData);
    res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE, userValues)); 
    return next();
}

async function forgotPassword(req, res, next){
   const{
       email
   } = req.body;

    try{
    const user = getValues(await User.findOne({
        where : {user_email : email}
    }));

    if(_.isNull(user)){
        res.send(200,setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE, cnf.ERR.NOT_FOUND));
        return next();
    }

    User.update({
        account_status: false
    },{
        where : {id : user.id}
    });
    var resetKey = generateToken(user, 'password');
    var parts = req.url.split('/');
    var url = "http://"+req.headers.host+'/'+parts[1]+'/'+parts[2]+'/password-reset?key='+resetKey;
    console.log(url);
    res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE,cnf.SUCCESS.SUCCESS_MESSAGE));
    return next();
    
   }catch(error) {
       console.log(error);
        return next(getError(error));
   }
}
async function changePassword(req, res, next){
    const {
        new_password,
        confirmNewPassword
    } = req.body;

    const {
        id
    } = req.User;

    if(!_.isEqual(new_password,confirmNewPassword)){
        return next(new err.InvalidCredentialsError("password mismatch"));
    }
    var newP = bcrypt.hashSync(new_password, cnf.SALT_ROUND);
    try{
        await User.update({
            user_password: newP
        },{
            where : {id : id}
        });

        res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE,cnf.SUCCESS.SUCCESS_MESSAGE));
        return next();
    }catch(error){
        return next(getError(error));
    }
}
async function resetPassword(req, res, next){
    const{
        new_password
    } = req.body;
    const key = parseQuery(req).key;
    
    try{
        var { data } = jwt.verify(key, cnf.PASSWORD_RESET_SECRET);

        const userData = getValues(await User.findOne({
            where: {user_email: data.email}
        }));
        
        if(_.isNull(userData) || !_.isEqual(userData.user_name,data.name)){
            return next(new err.UnauthorizedError('unauthorized attempt'));
        }

        var newP = bcrypt.hashSync(new_password,cnf.SALT_ROUND);

        await User.update({
            account_status: true,
            user_password: newP
        },{
            where : {id : userData.id}
        });

        res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE,cnf.SUCCESS.SUCCESS_MESSAGE));
        return next();
    } catch(error){
        if(_.isEqual(error.name,'JsonWebTokenError')){
            return next(new err.RequestExpiredError("link expired, try again"));
        } else{
            return next(getError(error));
        }
    }

}

async function confirmEmail(req, res, next){
    const key = parseQuery(req).key;

    try{
        var { data } = jwt.verify(key, cnf.CONFIRM_EMAIL_SECRET);
        const userData = getValues(await User.findOne({
            where : {user_email : data.email}
        }));

        if(_.isNull(userData)){
            return next(new err.UnauthorizedError('unauthorized attempt'));
        }

        User.update({
            user_email_verified : true
        },{
            where : {user_email : data.email}
        });
        res.send(200, setReturnObject(200,cnf.SUCCESS.SUCCESS_CODE, cnf.SUCCESS.SUCCESS_MESSAGE));
    } catch(error){
        if(_.isEqual(error.name,'JsonWebTokenError')){
            return next(new err.RequestExpiredError("link expired, try again"));
        } else{
            return next(getError(error));
        }
    }
}

async function getProfileDetails(req, res, next){
    const{
        id
    } = parseQuery(req);
    
    try{
        const profile = getValues(await UserProfile.findOne({
            where : {user_id : id}
        }));
        res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE, profile));
        return next();
    }catch(error){
        return next(getError(error)); 
    }
}
async function setProfileDetails(req, res, next){
    const{
        user_id,
        first_name,
        last_name,
        dob,
        tag_pref
    } = req.body;

    if(_.isNull(user_id) || _.isNull(first_name) || _.isNull(last_name) || _.isNull(dob) || _.isEmpty(tag_pref)){
        res.send(200, setReturnObject(200, cnf.ERR.FAILURE_CODE, cnf.ERR.INVALID));
        return next();
    }

    try{
       // Update profile
       var today = new Date();
       var birthDate = new Date(dob);
       var age =  today.getFullYear()-birthDate.getFullYear();
       
       var m = today.getMonth() - birthDate.getMonth();
       
       if(m < 0  || (m===0 && today.getDate() < birthDate.getDate())){
           age--;
       }
       
        UserProfile.update({
            firstname   :   first_name,
            lastname    :   last_name,
            dob         :   dob,
            age         :   age,
            tag_pref    :   tag_pref,
            updated_at  :   new Date()
        },{
            where : {user_id : user_id}
        });
        res.send(200, setReturnObject(200, cnf.SUCCESS.SUCCESS_CODE, cnf.SUCCESS.SUCCESS_MESSAGE));
        return next();
    } catch(error){
        console.log(error);
        return next(getError(error));
    }


}
async function sendEmailTest(){
    console.log("HERE");
    const payload = {
        from : 'test@tournap.com',
        to : 'jagyadeep@gmail.com'
    }
    sendEmail(payload);
}
module.exports = {
    signUp,
    signIn,
    getUsers,
    forgotPassword,
    resetPassword,
    changePassword,
    confirmEmail,
    sendEmailTest,
    setProfileDetails,
    getProfileDetails
};