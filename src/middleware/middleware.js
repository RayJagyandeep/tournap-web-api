const { User } = require('../../models/index');
var _ = require('lodash');
var bcrypt = require('bcrypt');
const { getValues } = require('sequelize-values')();
var err = require('restify-errors');
var validator = require('validator');
var jwt = require('jsonwebtoken');
var cnf = require('../../config/appConfig');
const { getError } = require('../helper/utils');

async function authSignUp(req, res, next){
    const {
        email,
    } = req.body;
    // Implement Empty Check for all fields
    try{
        var userData = await User.findOne({
            where:{user_email : email}
         });
     
         if(_.isNull(userData)){
             return next();
         } else{
             return next(new err.ConflictError(cnf.ERR.DUPLICATE));
         }
    } catch(error){
        console.log(error);
        return next(getError(error));
    }
    
}

async function authSignIn(req, res, next) {
    const {
        email,
        password
    } = req.body;
   
    if(_.isEmpty(email) || _.isEmpty(password) || !validator.isEmail(email)){
        return next(new err.NotAcceptableError(cnf.ERR.INVALID));
    }

    try{
        var UserData = getValues(await User.findOne({
            where : {user_email : email}
        }));
    
        if(_.isNull(UserData)){
            return next(new err.ResourceNotFoundError(cnf.ERR.NOT_FOUND));
        }
       
        if(!UserData.account_status){
            return next(new err.LockedError("Account is temporarily locked. Please reset your password and try again !"));
        }

        if(!UserData.user_email_verified){
            return next(new err.UnauthorizedError("email not confirmed yet. Please confirm email"));
        }
        if(!bcrypt.compareSync(password, UserData.user_password)){
            return next(new err.InvalidCredentialsError(cnf.ERR.INVALID));
        }
        req.User = UserData;
        return next();
    } catch(error){
        return next(getError(error));
    }
    

}

async function auth(req,res,next){

    if(_.isEmpty(req.headers['authorization'])){
        return next(new err.InvalidHeaderError(cnf.ERR.INVALID));
    }
    var token = req.headers['authorization'];
    try{
        const{
            data
        } = jwt.verify(token,cnf.SECRET_KEY);

        var userData = getValues(await User.findOne({
            where       :   {id :   data.id},
            attributes  :   ['user_email']
        }));
       
        if(_.isNull(userData) || !_.isEqual(userData.user_email, data.email)){
            return next(new err.NotAuthorizedError(cnf.ERR.UNAUTHORIZED));
        }
        req.User = userData;
        return next();

    } catch(error){
        return next(getError(error));
    }
   
}

async function authRenew(req, res, next) {

    if(_.isEmpty(req.headers['authorization'])){
        return next(new err.InvalidHeaderError(cnf.ERR.INVALID));
    }
    var token = req.headers['authorization'];

    try{
        var userData = getValues(await User.findOne({
            where : {user_tn_token: token}
        }));

        if(_.isNull(userData)){
            return next(new err.UnauthorizedError(cnf.ERR.UNAUTHORIZED));
        }
        const { data } = jwt.verify(token,cnf.REFRESH_SECRET);
        if(!(_.isEqual(userData.id,data.id)) ||  !(_.isEqual(userData.user_name,data.userName)) || !(_.isEqual(userData.user_email,data.email)))
        {
            return next(new err.UnauthorizedError(cnf.ERR.UNAUTHORIZED));
        }
        req.User = userData;
        return next();
    } catch(error){
        return next(getError(error));
    }
}
module.exports = {
    authSignUp,
    authSignIn,
    auth,
    authRenew
}