var url = require('url');
var jwt = require('jsonwebtoken');
var cnf = require('../../config/appConfig');
var ERR = require('restify-errors');
const nodemailer = require('nodemailer');

module.exports = {
    setReturnObject: (statusCode, code ,data) => {
        let returnObject = {
            code:"",
            status: "",
            data: ""
        };
        returnObject.code = code;
        returnObject.status = statusCode;
        returnObject.data = data;
        return returnObject;
    },

    parseQuery : (req) => {
        const query = url.parse(req.url, true).query;
        return query;
    },

    getError: (error) => {
        console.log(error.name);
        switch (error.name) {
            case 'TokenExpiredError':
                return new ERR.UnauthorizedError(cnf.ERR.EXPIRED);
            case 'SequelizeDatabaseError':
                return new ERR.InternalServerError(cnf.ERR.INTERNAL_ERROR);
            case 'JsonWebTokenError':
                return new ERR.UnauthorizedError(cnf.ERR.UNAUTHORIZED);
            case 'ReferenceError':
                return new ERR.InternalServerError(cnf.ERR.INTERNAL_ERROR);
            default:
                return error.name;
        }
    },
    generateToken: (data, type) => {
        var EXPIRES_IN = "";
        var SECRET_KEY = "";
        var PAYLOAD = {};
        switch (type) {
            case 'access':
                EXPIRES_IN = cnf.ACCESS_TOKEN_EXPIRY;
                SECRET_KEY = cnf.SECRET_KEY;
                PAYLOAD.id = data.id;
                PAYLOAD.userName = data.user_name;
                PAYLOAD.email = data.user_email;
                PAYLOAD.email_verified = data.is_email_verified;
                break;
            case 'refresh':
                EXPIRES_IN = cnf.REFRESH_TOKEN_EXPIRY;
                SECRET_KEY = cnf.REFRESH_SECRET;
                PAYLOAD.id = data.id;
                PAYLOAD.userName = data.user_name;
                PAYLOAD.email = data.user_email;
                break;
            case 'password':
                EXPIRES_IN = cnf.PASSWORD_RESET_EXPIRY;
                SECRET_KEY = cnf.PASSWORD_RESET_SECRET;
                PAYLOAD.name = data.user_name;
                PAYLOAD.email = data.user_email;
                break;
            case 'confirmEmail':
                EXPIRES_IN = cnf.CONFIRM_EMAIL_EXPIRY;
                SECRET_KEY = cnf.CONFIRM_EMAIL_SECRET;
                PAYLOAD.email = data.email;
                break;
            default:
                return null;
        }
        
        var token = jwt.sign({
            data: PAYLOAD
        }, SECRET_KEY, {expiresIn : EXPIRES_IN});
        return token;
    },

    sendEmail: async(payload) => {
        console.log("HERE1");
        try{
            let testAccount = await nodemailer.createTestAccount();

            let transporter = nodemailer.createTransport({
                host : 'smtp.ethereal.email',
                port : 465,
                secure: true,
                proxy: 'https://10.158.100.6:8080/',
                auth : {
                    user : testAccount.user,
                    pass : testAccount.pass
                } 
            });
    
            let info = await transporter.sendMail({
                from : payload.from,
                to :    payload.to,
                subject : 'Hello',
                text: 'Hello world',
                html: 'hello world'
            });
            console.log("HERE2");
            console.log('Message sent :'+info.messageId);
            console.log('Preview url :'+nodemailer.getTestMessageUrl(info));
        }catch(error){
            console.log(error);
        }
    }
}