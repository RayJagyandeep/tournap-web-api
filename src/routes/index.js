var Router = require('restify-router').Router;
var routes = new Router();

var v1 = require('./v1/index');

routes.add('/v1',v1);

module.exports = routes;
