var Router = require('restify-router').Router;
var routes  = new Router();
var {authSignUp, authSignIn, auth, authRenew} = require('../../middleware/middleware');
const controllers = require('../../controllers/index');

const { 
    User
 } = controllers.v1;

//routes.get('/api/:name', name.getName);
routes.post('/api/register', authSignUp, User.signUp);
routes.post('/api/login', authSignIn, User.signIn);
routes.get('/api/hello', auth, User.getUsers);
routes.get('/api/renew', authRenew, User.signIn);
routes.post('/api/forgotPassword',User.forgotPassword);
routes.post('/api/password-reset',User.resetPassword);
routes.get('/api/confirm-email',User.confirmEmail);
routes.get('/api/sendEmail', User.sendEmailTest);
routes.put('/api/create-profile', User.setProfileDetails);
routes.get('/api/get-profile', User.getProfileDetails);
module.exports = routes;